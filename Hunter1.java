package myrobot;


import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;

import java.awt.*;

public class Hunter1 extends AdvancedRobot {
    Enemy enemy = new Enemy();

    public static double PI = Math.PI;


    public void run() {
        //如果 flag 被设置成 true，那么坦克车转动时，炮保持原来的方向。
        setAdjustGunForRobotTurn(true);
        //如果 flag 被设置成 true，那么坦克车（和炮）转动时，雷达会保持原来的方向。
        setAdjustRadarForGunTurn(true);
        //设置颜色
        this.setColors(Color.red, Color.blue, Color.yellow, Color.black, Color.green);

        //循环
        while (true) {
            //判断敌人名字是否为空
            if (enemy.name == null) {
                setTurnRadarRightRadians(2 * PI);
                execute();
            } else {
                execute();

            }

        }

    }


    public void onScannedRobot(ScannedRobotEvent e) {
        enemy.update(e, this);
        double Offset = rectify(enemy.direction - getRadarHeadingRadians());
        setTurnRadarRightRadians(Offset * 1.5);

    }

    //角度修正方法，重要

    public double rectify(double angle) {
        if (angle < -Math.PI)

            angle += 2 * Math.PI;

        if (angle > Math.PI)

            angle -= 2 * Math.PI;

        return angle;

    }

    public class Enemy {
        public double x, y;

        public String name = null;

        public double headingRadian = 0.0D;

        public double bearingRadian = 0.0D;

        public double distance = 1000D;

        public double direction = 0.0D;

        public double velocity = 0.0D;

        public double prevHeadingRadian = 0.0D;

        public double energy = 100.0D;


        public void update(ScannedRobotEvent e, AdvancedRobot me) {
            name = e.getName();

            headingRadian = e.getHeadingRadians();

            bearingRadian = e.getBearingRadians();

            this.energy = e.getEnergy();

            this.velocity = e.getVelocity();

            this.distance = e.getDistance();

            direction = bearingRadian + me.getHeadingRadians();

            x = me.getX() + Math.sin(direction) * distance;

            y = me.getY() + Math.cos(direction) * distance;

        }

    }
}
